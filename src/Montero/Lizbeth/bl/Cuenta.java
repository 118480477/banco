package Montero.Lizbeth.bl;

import java.time.LocalDate;

public class Cuenta {

    protected int numeroCuenta;

    protected double saldo;

    protected LocalDate fechaApertura;



    /**
     *Constructor por defecto,para hacer casos de prueba.
     * Clase que representa la abstraccion del cliente
     */
    public Cuenta() {
    }

    /**
     * Constructor que recibe por parametros los atributos de la clase
     * @param numeroCuenta  the numero cuenta
     * @param saldo         the saldo
     * @param fechaApertura the fecha apertura
     */
    public Cuenta(int numeroCuenta, double saldo, LocalDate fechaApertura) {
        this.numeroCuenta = numeroCuenta;
        this.saldo = saldo;
        this.fechaApertura = fechaApertura;
    }

    /**
     * Metodo que recibe por parametro el numero de la cuenta
     *
     * @return the numero cuenta
     */
    public int getNumeroCuenta() {
        return numeroCuenta;
    }

    /**
     * Metodo que recibe por parametro el numero de la cuenta
     *
     * @param numeroCuenta the numero cuenta
     */
    public void setNumeroCuenta(int numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    /**
     * Metodo que recibe por parametro el saldo de la cuenta
     *
     * @return the saldo
     */
    public double getSaldo() {
        return saldo;
    }

    /**
     * Metodo que recibe por parÃ¡metro el saldo de la cuenta
     *
     * @param saldo the saldo
     */
    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    /**
     * Metodo que recibe por parametro la fecha de apertura de la cuenta
     *
     * @return la fecha apertura
     */
    public LocalDate getFechaApertura() {
        return fechaApertura;
    }

    /**
     * Metodo que recibe por parametro la fecha de apertura de la cuenta
     *
     * @param fechaApertura the fecha apertura
     */
    public void setFechaApertura(LocalDate fechaApertura) {
        this.fechaApertura = fechaApertura;
    }
    /**
     * Metodo que ayuda a concatenar  y guardar cada atributo separado de una coma.
     * @return la informacion concatenada y separado por coma.
     */
    @Override
    public String toString() {
        return "Cuenta{" +
                "numeroCuenta=" + numeroCuenta +
                ", saldo=" + saldo +
                ", fechaApertura=" + fechaApertura +
                '}';
    }

    /**
     * metodo de comprobacion si un objeto es igual que otro (busqueda)
     * @param o Object
     * @return true si encuentra el atributo a buscar.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cuenta c = (Cuenta) o;
        return getNumeroCuenta() == c.getNumeroCuenta();
    }

    /**
     * metodo que cumple la misma funcion del tostring solo que sperado por comas
     * @return informaciÃ³n concatenada y separada por comas.
     */
    public String infoToCSV(){
        return numeroCuenta + " , " + saldo + " , " + fechaApertura ;
    }


}


