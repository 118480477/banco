package Montero.Lizbeth.bl;

import Montero.Lizbeth.bl.Cuenta;

import java.time.LocalDate;

public class CuentaCorriente extends Cuenta {

    /**
     *Constructor por defecto,para hacer casos de prueba.
     * Clase que representa la abstraccion del cliente
     */
    public CuentaCorriente() {
        super();
    }

    /**
     * Constructor que recibe por parametros los atributos de la clase
     * @param numeroCuenta  the numero cuenta
     * @param saldo         the saldo
     * @param fechaApertura the fecha apertura
     */
    public CuentaCorriente(int numeroCuenta, double saldo, LocalDate fechaApertura) {
        super(numeroCuenta, saldo, fechaApertura);
    }
    /**
     * Metodo que ayuda a concatenar  y guardar cada atributo separado de una coma.
     * @return la informacion concatenada y separado por coma.
     */
    @Override
    public String toString() {
        return "CuentaCorriente{" +
                "numeroCuenta=" + numeroCuenta +
                ", saldo=" + saldo +
                ", fechaApertura=" + fechaApertura +
                '}';
    }

    /**
     * metodo que cumple la misma funcion del tostring solo que sperado por comas
     * @return informacion concatenada y separada por comas.
     */
    @Override
    public String infoToCSV(){
        return super.infoToCSV() ;
    }


}
