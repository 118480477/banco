package Montero.Lizbeth.bl;

import Montero.Lizbeth.bl.Cuenta;

import java.time.LocalDate;

public class CuentaProgramada extends Cuenta {
    private int cuentacAsociada;
    private double ahorro;

    /**
     *Constructor por defecto,para hacer casos de prueba.
     * Clase que representa la abstraccion del cliente
     */
    public CuentaProgramada() {
        super();
    }

    /**
     * Constructor que recibe por parametros los atributos de la clase
     * @param numeroCuenta  the numero cuenta
     * @param saldo         the saldo
     * @param fechaApertura the fecha apertura
     * @param cuentacAsociada    the cc asociada
     * @param ahorro        the ahorro
     */
    public CuentaProgramada(int numeroCuenta, double saldo, LocalDate fechaApertura, int cuentacAsociada,
                            double ahorro) {
        super(numeroCuenta, saldo, fechaApertura);
        this.cuentacAsociada = cuentacAsociada;
        this.ahorro = ahorro;
    }

    /**
     * Metodo que retorna el atributo contrato de la cuenta corriente asociada
     * @return the cc asociada
     */
    public int getcuentacAsociada() {
        return cuentacAsociada;
    }

    /**
     * Metodo que recibe por parÃ¡metro cc asociada.
     *
     * @param ccAsociada the cc asociada
     */
    public void setcuentacAsociada(int ccAsociada) {
        this.cuentacAsociada = ccAsociada;
    }

    /**
     * Metodo que retorna el atributo ahorro de la cuenta corriente asociada
     *
     * @return the ahorro
     */
    public double getAhorro() {
        return ahorro;
    }

    /**
     * Metodo que recibe por parÃ¡metro el ahorro de la cuenta corriente.
     *
     * @param ahorro the ahorro
     */
    public void setAhorro(double ahorro) {
        this.ahorro = ahorro;
    }
    /**
     * Metodo que ayuda a concatenar  y guardar cada atributo separado de una coma.
     * @return la informacion concatenada y separado por coma.
     */
    @Override
    public String toString() {
        return "CuentaProgramada{" +
                "numeroCuenta=" + numeroCuenta +
                ", saldo=" + saldo +
                ", fechaApertura=" + fechaApertura +
                ", cuentacAsociada=" + cuentacAsociada +
                ", ahorro=" + ahorro +
                '}';
    }
    /**
     * metodo que cumple la misma funcion del tostring solo que sperado por comas
     * @return informacion concatenada y separada por comas.
     */
    @Override
    public String infoToCSV(){
        return super.infoToCSV() + " , " +cuentacAsociada+ " , "+ ahorro;
    }
}
