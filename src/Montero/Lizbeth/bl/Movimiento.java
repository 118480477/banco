package Montero.Lizbeth.bl;

import java.time.LocalDate;

public class Movimiento {
    private String tipoMovimiento;
    private LocalDate fecha;
    private String descripcion;
    private double monto;

    /**
     *Constructor por defecto,para hacer casos de prueba.
     * Clase que representa la abstraccion del cliente
     */
    public Movimiento() {
    }

    /**
     * Constructor que recibe por parÃ¡metros los atributos de la clase
     *
     * @param tipoMovimiento the tipo movimiento
     * @param fecha          the fecha
     * @param descripcion    the descripcion
     * @param monto          the monto
     */
    public Movimiento(String tipoMovimiento, LocalDate fecha, String descripcion, double monto) {
        this.tipoMovimiento = tipoMovimiento;
        this.fecha = fecha;
        this.descripcion = descripcion;
        this.monto = monto;
    }

    /**
     * Metodo que retorna el atributo movimiento
     *
     * @return the tipo movimiento
     */
    public String getTipoMovimiento() {
        return tipoMovimiento;
    }

    /**
     * Metodo que recibe por parametro el movimiento
     *
     * @param tipoMovimiento the tipo movimiento
     */
    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    /**
     * Metodo que retorna el atributo fecha
     *
     * @return the fecha
     */
    public LocalDate getFecha() {
        return fecha;
    }

    /**
     * Metodo que recibe por parametro la fecha
     *
     * @param fecha the fecha
     */
    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    /**
     * Metodo que retorna el atributo descripcion
     *
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Metodo que recibe por parametro la descripcion
     *
     * @param descripcion the descripcion
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * Metodo que retorna el atributo monto
     *
     * @return the monto
     */
    public double getMonto() {
        return monto;
    }

    /**
     * Metodo que recibe por parametro el monto
     *
     * @param monto the monto
     */
    public void setMonto(double monto) {
        this.monto = monto;
    }

    /**
     * Metodo que ayuda a concatenar  y guardar cada atributo separado de una coma.
     * @return la informacion concatenada y separado por coma.
     */
    @Override
    public String toString() {
        return "Movimiento{" +
                "tipoMovimiento='" + tipoMovimiento + '\'' +
                ", fecha=" + fecha +
                ", descripcion='" + descripcion + '\'' +
                ", monto=" + monto +
                '}';
    }

    /**
     * metodo que cumple la misma funcion del tostring solo que sperado por comas
     * @return informacion concatenada y separada por comas.
     */
    public String infoToCSV() {
        return tipoMovimiento + " , " + fecha + " , " + descripcion + " , " + monto;
    }
}
