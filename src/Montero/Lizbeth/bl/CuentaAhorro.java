package Montero.Lizbeth.bl;

import Montero.Lizbeth.bl.Cuenta;

import java.time.LocalDate;

public class CuentaAhorro extends Cuenta {
    private double tasaInteres;

    /**
     *Constructor por defecto,para hacer casos de prueba.
     * Clase que representa la abstraccion del cliente
     */
    public CuentaAhorro() {
        super();
    }

    /**
     * Constructor que recibe por parametros los atributos de la clase
     * @param numeroCuenta  the numero cuenta
     * @param saldo         the saldo
     * @param fechaApertura the fecha apertura
     * @param tasaInteres   the tasa interes
     */
    public CuentaAhorro(int numeroCuenta, double saldo, LocalDate fechaApertura, double tasaInteres) {
        super(numeroCuenta, saldo, fechaApertura);
        this.tasaInteres = tasaInteres;
    }

    /**
     * Metodo que retorna el atributo tasa de interes
     *
     * @return the tasa interes
     */
    public double getTasaInteres() {
        return tasaInteres;
    }

    /**
     * Metodo que recibe por parametro la tasa de interes.
     *
     * @param tasaInteres the tasa interes
     */
    public void setTasaInteres(double tasaInteres) {
        this.tasaInteres = tasaInteres;
    }

    /**
     * Metodo que ayuda a concatenar  y guardar cada atributo separado de una coma.
     * @return la informacion concatenada y separado por coma.
     */
    @Override
    public String toString() {
        return "CuentaAhorro{" +
                "numeroCuenta=" + numeroCuenta +
                ", saldo=" + saldo +
                ", fechaApertura=" + fechaApertura +
                ", tasaInteres=" + tasaInteres +
                '}';
    }

    /**
     * metodo que cumple la misma funcion del tostring solo que sperado por comas
     * @return informaciÃ³n concatenada y separada por comas.
     */
    @Override
    public String infoToCSV(){
        return super.infoToCSV() + " , " + tasaInteres;
    }
}
