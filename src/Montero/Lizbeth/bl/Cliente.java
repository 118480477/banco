package Montero.Lizbeth.bl;

import java.util.Objects;

public class Cliente {
    private String nombre, id , direccion;

    /**
     *Constructor por defecto,para hacer casos de prueba.
     * Clase que representa la abstraccion del cliente
     */
    public Cliente() {
    }

    /**
     * Constructor que recibe por parametros los atributos de la clase
     * @param nombre    the nombre
     * @param id        the id
     * @param direccion the direccion
     */
    public Cliente(String nombre, String id, String direccion) {
        this.nombre = nombre;
        this.id = id;
        this.direccion = direccion;
    }

    /**
     * Metodo que retorna el atributo nombre del cliente
     *
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo que recibe por parÃ¡metro el nombre del cliente
     *
     * @param nombre the nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Metodo que retorna el atributo id del cliente
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Metodo que recibe por parmetro el id del cliente
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Metodo que retorna el atributo direccion del cliente
     *
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Metodo que recibe por parÃ¡metro la direccion del cliente
     *
     * @param direccion the direccion
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    /**
     * Metodo que ayuda a concatenar  y guardar cada atributo separado de una coma.
     * @return la informacion concatenada y separado por coma.
     */
    @Override
    public String toString() {
        return "Cliente{" +
                "nombre='" + nombre + '\'' +
                ", id='" + id + '\'' +
                ", direccion='" + direccion + '\'' +
                '}';
    }
    /**
     * metodo de comprobacion si un objeto es igual que otro (busqueda)
     * @param o Object
     * @return true si encuentra el atributo a buscar.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente c = (Cliente) o;
        return Objects.equals(getId(), c.getId());
    }

    /**
     * metodo que cumple la misma funcion del tostring solo que sperado por comas
     * @return informaciÃ³n concatenada y separada por comas.
     */
    public String infoToCSV(){
        return nombre + " , " + id + " , " + direccion ;
    }




}


